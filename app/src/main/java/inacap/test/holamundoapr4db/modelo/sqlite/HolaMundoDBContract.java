package inacap.test.holamundoapr4db.modelo.sqlite;

import android.provider.BaseColumns;

/**
 * Created by mitlley on 28-08-17.
 */

public class HolaMundoDBContract {
    private HolaMundoDBContract(){}

    public static class HolaMundoDBUsuarios implements BaseColumns {
        public static final String TABLE_NAME = "usuarios";
        public static final String COLUMN_NAME_USERNAME = "username";
        public static final String COLUMN_NAME_PASSWORD = "password";
    }
}
